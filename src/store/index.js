import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
      count: 0,
      todos: [
          {name: "buy milk", isDone: true, id: 1},
          {name: "study math", isDone: false, id: 2},
          {name: "Art class", isDone: false, id: 3},
      ]
    },
    getters: {
        getDoneTodos: state => state.todos.filter(todo => todo.isDone),
        getUndoneTodos: state => state.todos.filter(todo => !todo.isDone)
    },
    mutations: {
      increment (state) {
        state.count++
      },
      changeStatus(state, payload) {
        state.todos = state.todos.map( todo => {
            if(todo.id === payload){
                todo.isDone = !todo.isDone;
            }

            return todo;
        });
      }
    }
  })

  export default store