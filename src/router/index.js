import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Home1 from "../views/Home1.vue"
import Home2 from "../views/Home2.vue"
import Todos from "../views/Todos.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/home",
    name: 'home',
    component: HomeView,
    children: [
      {
        path: "/home",
        component: Home1
      },
      {
        path: "/home2",
        component: Home2
      }

    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: "/todos",
    name: "todos",
    component: Todos
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
